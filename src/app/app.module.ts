import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgxUploaderModule } from 'ngx-uploader';
import { HighlightModule } from 'ngx-highlightjs';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgxUploaderModule,
    HighlightModule.forRoot({'theme': 'zenburn'}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
