import { Component, EventEmitter } from '@angular/core';

import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions, UploadStatus } from 'ngx-uploader';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  code_output = '{"data":{"url":"https://mz.ci/0x728A94","expired_at":"2018-06-12T12:43:32.586164+00:00"}}';
  // result: any;

  // file uploader
  // formData: FormData;
  humanizeBytes: Function;
  dragOver: boolean;

  uploadFile: UploadFile;
  uploadInput: EventEmitter<UploadInput>;
  uploadOptions: UploaderOptions;

  constructor() {
    this.uploadFile = null;
    this.uploadInput = new EventEmitter<UploadInput>();
    this.uploadOptions = { concurrency: 1 };
    this.humanizeBytes = humanizeBytes;
  }

  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'allAddedToQueue') {
      if (this.uploadFile && this.uploadFile.progress.data && this.uploadFile.progress.data.percentage === 0) {
        const event: UploadInput = {
          type: 'uploadAll',
          // url: 'https://ngx-uploader.com/upload',
          url: 'https://mz.ci/0x0',
          method: 'POST'
        };

        this.uploadInput.emit(event);
        console.info(`[onUploadOutput]${this.uploadFile.name} is uploading`);
      }
    } else if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') {
      if (this.uploadFile == null || (this.uploadFile.progress.data && this.uploadFile.progress.data.percentage === 100)) {
        this.uploadFile = output.file;

        console.info(`[onUploadOutput]${output.file.name} is selected`);
      }
    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
      // const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
      // this.files[index] = output.file;
    } else if (output.type === 'removed') {
      // this.files = this.files.filter((file: UploadFile) => file !== output.file);
    } else if (output.type === 'dragOver') {
      this.dragOver = true;
    } else if (output.type === 'dragOut') {
      this.dragOver = false;
    } else if (output.type === 'drop') {
      this.dragOver = false;
    } else if (output.type === 'rejected' && typeof output.file !== 'undefined') {
      console.log(output.file.name + ' rejected');
    } else if (output.type === 'done') {
      console.log(`[onUploadOutput]${output.file.name} done`);

      // this.code_output = JSON.stringify(output.file.response);
      // this.uploadFile = output.file;

      // FIXME remove blew line
      // output.file.response = JSON.parse(this.code_output);
      this.code_output = JSON.stringify(output.file.response);

      console.debug(`[onUploadOutput]${output.file.name} response ${output.file.response}`);
    }

    console.log(`[onUploadOutput]event type ${output.type}`);
    // this.files = this.files.filter(file => file.progress.status !== UploadStatus.Done);
  }

  // startUpload(): void {
  //   const event: UploadInput = {
  //     type: 'uploadAll',
  //     url: 'https://ngx-uploader.com/upload',
  //     method: 'POST',
  //     data: { foo: 'bar' }
  //   };

  //   this.uploadInput.emit(event);
  // }

  cancelUpload(): void {
    const id = this.uploadFile.id;
    this.uploadInput.emit({ type: 'cancel', id: id });
    this.uploadInput.emit({ type: 'remove', id: id });
    this.uploadFile = null;
  }

  copyIt(dat: string): void {
    const textarea = document.createElement('textarea');
    textarea.style.opacity = '0';
    textarea.style.position = 'fixed';

    textarea.innerText = dat;
    document.body.appendChild(textarea);
    textarea.select();

    try {
      const successful = document.execCommand('copy');
      if (!successful) { throw new Error('unkown'); }
    } catch (err) {
      window.prompt('Copy to clipboard: Ctrl+C, Enter', dat);
    }

    textarea.remove();
  }
}
